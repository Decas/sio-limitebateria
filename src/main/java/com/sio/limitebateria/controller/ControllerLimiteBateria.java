package com.sio.limitebateria.controller;

import com.sio.limitebateria.model.entity.LimitesBateria;
import com.sio.limitebateria.model.service.IServiceLimiteBaterias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ControllerLimiteBateria {
    @Autowired
    private IServiceLimiteBaterias baterias;

    @GetMapping("/listarLimiteBaterias")
    @ResponseStatus(HttpStatus.FOUND)
    public List<LimitesBateria> listarAll(){

        return  this.baterias.findByAll();
    }

    @GetMapping("/verLimiteBaterias/{id}")
    @ResponseStatus(HttpStatus.FOUND)
    public LimitesBateria listarId(@PathVariable long id){
        return this.baterias.findById(id);
    }

    @GetMapping("/verLimiteBateriasNombre/{nombre}")
    @ResponseStatus(HttpStatus.FOUND)
    public LimitesBateria listarNombre(@PathVariable String nombre){

        return this.baterias.findByNombre(nombre);
    }

    @PostMapping("/crearLimiteBaterias")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public LimitesBateria agregar(@RequestBody LimitesBateria limitesBateria) throws Exception {
        return this.baterias.save(limitesBateria);
    }

    @PutMapping("/editarLimiteBaterias/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public LimitesBateria editar(@PathVariable long id, @RequestBody LimitesBateria limitesBateria) throws Exception {
        return this.baterias.editById(id,limitesBateria);
    }

    @PutMapping("/estadoLimiteBaterias/{id}/estado/{estado}")
    @ResponseStatus(HttpStatus.CREATED)
    public LimitesBateria editarEstado(@PathVariable long id, @PathVariable String estado, @RequestBody LimitesBateria limitesBateria ) throws Exception {
      return this.baterias.editByEstado(id,estado,limitesBateria);
    }


}
