package com.sio.limitebateria.model.repository;

import com.sio.limitebateria.model.entity.LimitesBateria;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ILimitesBateriaDao extends CrudRepository< LimitesBateria, Long> {


    public LimitesBateria findByNombre(String s);
    public LimitesBateria findByIdAndEstado(long id,boolean estado);


}
