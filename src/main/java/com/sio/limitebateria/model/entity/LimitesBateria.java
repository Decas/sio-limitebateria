package com.sio.limitebateria.model.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;

@Table(name = "limites")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LimitesBateria implements Serializable {
       @Id
       @GeneratedValue(strategy = GenerationType.IDENTITY)
       private long id;
       @Column(name = "codigo",nullable = false, unique = true)
       private String codigo;
       @Column(name = "nombre",nullable = false, unique = true)
       private String nombre;
       @Column(name = "descripcion")
       private String descripcion;
       @Column(name = "estado", nullable = false, columnDefinition = "boolean default true")
       private boolean estado;



}
