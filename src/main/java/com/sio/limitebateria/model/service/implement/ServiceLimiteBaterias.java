package com.sio.limitebateria.model.service.implement;

import com.sio.limitebateria.model.entity.LimitesBateria;
import com.sio.limitebateria.model.repository.ILimitesBateriaDao;
import com.sio.limitebateria.model.service.IServiceLimiteBaterias;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
public class ServiceLimiteBaterias implements IServiceLimiteBaterias {

    @Autowired
    private ILimitesBateriaDao dao;

    @Override
    @Transactional(readOnly = true)
    public List<LimitesBateria> findByAll() {
        return (List<LimitesBateria>) this.dao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public LimitesBateria findById(long id) {
        return this.dao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public LimitesBateria findByNombre(String nombre) {
        return this.dao.findByNombre(nombre);
    }

    @Override
    @Transactional
    public LimitesBateria save(LimitesBateria limitesBateria) throws Exception {
        if(limitesBateria.getCodigo().isEmpty()|| limitesBateria.getNombre().isEmpty()){
            throw new Exception("error en los datos");
        }
        return this.dao.save(limitesBateria);
    }

    @Override
    @Transactional
    public LimitesBateria editById(long id, LimitesBateria limitesBateria) throws Exception{
        LimitesBateria encontrado=this.dao.findById(id).orElse(null);
        encontrado.setNombre(limitesBateria.getNombre());
        encontrado.setCodigo(limitesBateria.getCodigo());
        encontrado.setDescripcion(limitesBateria.getDescripcion());
        if(encontrado.getNombre().isEmpty() || encontrado.getCodigo().isEmpty()){
            throw new Exception("error en los datos");
        }

        return this.dao.save(encontrado);
    }

    @Override
    @Transactional
    public LimitesBateria editByEstado(long id, String estado, LimitesBateria limitesBateria) throws Exception {
        LimitesBateria encontrado=this.dao.findByIdAndEstado(id,Boolean.valueOf(estado));
        if(encontrado==null){
            throw new Exception("no encontrado");
        }
        encontrado.setEstado(limitesBateria.isEstado());
        return encontrado;
    }
}
