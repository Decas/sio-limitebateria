package com.sio.limitebateria.model.service;

import com.sio.limitebateria.model.entity.LimitesBateria;
import java.util.List;

public interface IServiceLimiteBaterias {


      public List<LimitesBateria> findByAll();

      public LimitesBateria findById(long id);

      public LimitesBateria findByNombre(String nombre);

      public LimitesBateria save(LimitesBateria nueva) throws Exception;

      public LimitesBateria editById(long id, LimitesBateria limitesBateria) throws Exception;

      public LimitesBateria editByEstado(long id,String estado,LimitesBateria limitesBateria) throws Exception;



}
