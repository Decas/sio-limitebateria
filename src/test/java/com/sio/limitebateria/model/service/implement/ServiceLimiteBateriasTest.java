package com.sio.limitebateria.model.service.implement;


import com.sio.limitebateria.model.entity.LimitesBateria;
import com.sio.limitebateria.model.repository.ILimitesBateriaDao;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ServiceLimiteBateriasTest {

    @Mock
    private List<LimitesBateria> lista;
    @Mock
    private LimitesBateria limitesBateria;
    @Mock
    private ILimitesBateriaDao limitesBateriaDao;
    @Mock
    private LimitesBateria otro;

    @InjectMocks
    private ServiceLimiteBaterias serviceLimiteBaterias;

    @Before
    public  void setUp(){
        this.lista=new ArrayList<LimitesBateria>();
        this.limitesBateria=new LimitesBateria(1L,"11","muelle","",true);
        this.otro=new LimitesBateria();
        lista.add(this.limitesBateria);

    }

    @Test
    public void listarAllTest(){

        System.out.println("listar todos");
    when(this.limitesBateriaDao.findAll()).thenReturn(this.lista);
        Assertions.assertThat(1).isEqualTo(this.serviceLimiteBaterias.findByAll().size());

    }

    @Test
    public void listarIdTest(){
        System.out.println("listar id");
        when(this.limitesBateriaDao.findById(1L)).thenReturn(Optional.ofNullable(this.limitesBateria));
        Assertions.assertThat(this.limitesBateria).isEqualTo(this.serviceLimiteBaterias.findById(1L));
    }

    @Test
    public void listarNombreTest(){
        System.out.println("listar nombre");
        when(this.limitesBateriaDao.findByNombre("muelle")).thenReturn((this.limitesBateria));
        Assertions.assertThat(this.limitesBateria).isEqualTo(this.serviceLimiteBaterias.findByNombre("muelle"));
    }

    @Test
    public void crearTest() throws Exception{
        System.out.println("crear nuevo");
        LimitesBateria nuevo=new LimitesBateria(2L,"12","trampas","",true);
        when(this.limitesBateriaDao.save(nuevo)).thenReturn(nuevo);
        Assertions.assertThat(nuevo).isEqualTo(this.serviceLimiteBaterias.save(nuevo));

    }

    @Test(expected = Exception.class)
    public void crearExcepcionTest() throws Exception{
        System.out.println("crear nuevo, y espera una exception");
        //segun LN el codigo o el nombre deben ser not null para salvar
        LimitesBateria nuevo=new LimitesBateria(2L,"","","",true);
        //lenient para corregir el unecessary excption
        lenient().when(this.limitesBateriaDao.save(nuevo)).thenReturn(nuevo);
        Assertions.assertThat(nuevo).isEqualTo(this.serviceLimiteBaterias.save(nuevo));

    }

    @Test
    public void editarTest() throws Exception{
        System.out.println("editar test");

        lenient().when(this.limitesBateriaDao.findById(1L)).thenReturn(Optional.ofNullable(this.limitesBateria));
    this.limitesBateria.setCodigo("12");
    this.limitesBateria.setNombre("mulas");
    this.limitesBateria.setDescripcion("asd");
    this.serviceLimiteBaterias.editById(1L,this.limitesBateria);

    }

    @Test
    public void editEstadoTest() throws Exception {
        System.out.println("editarestado test");
        lenient().when(this.limitesBateriaDao.findByIdAndEstado(1L,true)).thenReturn(this.limitesBateria);
        this.limitesBateria.setEstado(false);
        this.serviceLimiteBaterias.editByEstado(1L,"true",this.limitesBateria);

    }

















}
